import {useEffect, useState , useRef} from 'react';
import axios from 'axios';

export default function App() {
  //listen on input:
  const [term , setTerm]=useState('');
  const [result, setResult]=useState([]);
const termUserRef= useRef();
const prevTerm=termUserRef.current;

useEffect(()=>{
  termUserRef.current= term
} )


  useEffect(() =>{
const search =async () =>{

  const respond = await axios.get('https://en.wikipedia.org/w/api.php',{
    params:{
      action:'query',
      list: 'search',
      origin:'*',
      format: 'json',
      srsearch: term,
    },
  });
  setResult(respond)

};
if(!result.length){

  if (term){
    search();
  }
}else if(prevTerm !== term ){
const debounceSearch= setTimeout( ()=>{
  if(term){
    search();
  }
} , 5000 );

return ()=>{
clearTimeout(debounceSearch);
};
}
 } , [term , result.length,prevTerm]);
const fetchResult =result?.data?.query?.search.map((el)=>{
return (
  <tr>
    <th scope='row' key={el.pageid}> 1 </th>
    <td> {el.title} </td> 
    <td> {el.snippet} </td> 
  </tr>
);
} ) ;
  return (
    <div className='container'>
      <div className='row'>
        <div className='col'>
          <div className='my-3'>
            <label htmlFor='exampleFormControlInput' className='form-label'>
              Search Input
            </label>
<input type='text' className='form-control bg-slate-300' id='exampleFormControlInput'
onChange={(e)=> setTerm(e.target.value)} value={term} />
          </div>
        </div>
      </div>

<div className='row'>
  <div className='col'>
    <table className='table'>
      <thead>
        <tr>
          <th scope='col'> # </th>
          <th scope='col'> Title </th>
          <th scope='col'> Desc </th>
        </tr>
</thead>
<tbody>
        { fetchResult }
        </tbody>
    </table>
  </div>
</div>

    </div>


  )
}



